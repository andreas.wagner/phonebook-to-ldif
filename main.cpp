#include <iostream>
#include <fstream>
#include  <string>
#include <regex>

int table_row = 2;

void print_entry(
	std::string & name,
	std::string & phone,
	std::string & sn)
{
	name = std::regex_replace(name, std::regex(","), "\\,");
	phone = std::regex_replace(phone, std::regex(","), "\\,");
	sn = std::regex_replace(sn, std::regex(","), "\\,");
	if(name.length() != 0)
		std::cout << "dn: cn=" << name << ",dc=rock64" << std::endl
			<< "changetype: add" << std::endl
			<< "objectClass: top" << std::endl
			<< "objectClass: person" << std::endl
			<< "telephoneNumber: " << phone << std::endl
			<< "sn: " << sn << std::endl
			<< std::endl;
}

void parse(std::ifstream &file)
{
	while(file.good())
	{
		std::string line, name, phone, sn;
		std::size_t
			pos = 0,
			old_pos = 0;
		std::getline(file, line);
		for(int i = 0; i < table_row && pos != std::string::npos; i++)
		{
			old_pos = pos;
			pos = line.find('\t', pos+1);
		}
		if(pos != std::string::npos)
		{
			name = std::string(line.c_str(), 0, old_pos);
			phone = std::string(
				line.c_str() + old_pos,
				pos-old_pos);
		}
		else
		{
			name = std::string(line.c_str(), 0, old_pos);
			phone = std::string(line.c_str() + old_pos +1);
		}

		std::size_t comma_pos = name.find(',');
		std::size_t
			sn_pos = 0,
			sn_oldpos = 0;
		while(sn_pos < comma_pos)
		{
			sn_oldpos = sn_pos;
			sn_pos = name.find(' ', sn_pos + 1);
		}
		if(comma_pos == std::string::npos)
		{
			sn = std::string(name, sn_oldpos);
		}
		else
		{
			sn = std::string(name, sn_oldpos+1, comma_pos-sn_oldpos-1);
		}
		print_entry(name, phone, sn);
	}
}

int main(int argc, char *argv[])
{
	if(argc < 2)
	{
		return -1;
	}
	for(int i = 1; i < argc; i++)
	{
		std::ifstream file(argv[i]);
		parse(file);
	}
}

